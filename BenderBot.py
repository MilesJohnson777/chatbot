import string
import time
import RPi.GPIO as GPIO
from Read import getMessage
from Socket import openSocket, sendMessage
from Init import joinRoom
from PinCommand import *

s = openSocket()
joinRoom(s)
readbuffer = ""

GPIO.setmode(GPIO.BCM)
#source switch pins
GPIO.setup(18, GPIO.OUT, initial=False)
GPIO.setup(27, GPIO.OUT, initial=False)
GPIO.setup(17, GPIO.OUT, initial=False)
GPIO.setup(22, GPIO.OUT, initial=False)
GPIO.setup(4, GPIO.OUT, initial=False)
#glich box pins
GPIO.setup(23, GPIO.OUT, initial=True)
GPIO.setup(24, GPIO.OUT, initial=True)
GPIO.setup(25, GPIO.OUT, initial=True)
GPIO.setup(5, GPIO.OUT, initial=True)
GPIO.setup(16, GPIO.OUT, initial=True)
GPIO.setup(19, GPIO.OUT, initial=True)
GPIO.setup(26, GPIO.OUT, initial=True)
GPIO.setup(20, GPIO.OUT, initial=True)
GPIO.setup(21, GPIO.OUT, initial=True)

#inverted logic due to relay boards being active low
def pinLow(num):
    GPIO.output(num, True)

#inverted logic.....
def pinHigh(num):
    GPIO.output(num, False)

def toggleState(num):
    GPIO.output(num, not GPIO.input(num))

try:
    reminderClock = 0

    while True:
        #read from chat
        readbuffer = readbuffer + s.recv(1024).decode('utf-8')
        temp = readbuffer.split('\n')
        readbuffer = temp.pop()

        for line in temp:
            message = getMessage(line)
            if 'PING :tmi.twitch.tv' in message:
                print('Sending PONG response to twitch servers.')
                s.send('PONG :tmi.twitch.tv'.encode('utf-8'))
                if reminderClock > 0:
                    reminderClock = reminderClock - 1

            if "$bend" in message:
                print(message)
                tempCommandOrder = []
                pinCommands = []

                #clean user input
                message = message.split('\r')
                message.pop()
                message = message[0]
                message = message.split(' ')
                for str in message:
                    if '$bend' in str:
                        inputToSep = []
                        inputToSep = str.split('$bend')
                        message.remove(str)
                        message.append(inputToSep[1])
                message.pop()

                #limit commands
                if len(message) > 7:
                    sendMessage(s, 'Too many bend commands. The limit is 7.')
                    break

                #translate commands into GPIO pins
                printableCommandString = ''
                for command in message:
                    for validcommand in camCommands:
                        if command == validcommand:
                            printableCommandString = printableCommandString + ' ' + command
                            tempCommandOrder.append(camCommands.get(validcommand))
                tempCommandOrder.sort()

                for command in tempCommandOrder:
                    for number in relatedPins:
                        if command == number:
                            pinCommands.append(relatedPins.get(number))
                #execute commands
                printableCommandString = printableCommandString.upper()
                sendMessage(s, 'BEEP! BOOP! NOW ACTIVATING THE' + printableCommandString + ' CIRCUITS!!!')
                for num in pinCommands:
                    if num == 18 or num ==27 or num == 17 or num == 22 or num == 4:
                        toggleState(num)
                        time.sleep(0.2)
                        toggleState(num)
                        time.sleep(0.2)
                    if num == 23 or num == 24 or num == 25 or num == 5 or num == 16 or num == 19 or num == 26 or num == 20 or num == 21:
                        pinHigh(num)

                pinCommands.reverse()
                if 18 in pinCommands:
                    pinCommands.remove(18)

                #giving the transition time to occur
                time.sleep(4)

                for num in pinCommands:
                    if num == 23 or num == 24 or num == 25 or num == 5 or num == 16 or num == 19 or num == 26 or num == 20 or num == 21:
                        pinLow(num)
                    if num == 27 or num == 17 or num == 22 or num == 4:
                        toggleState(num)
                        time.sleep(0.2)
                        toggleState(num)
                        time.sleep(0.2)
                sendMessage(s, 'BLIP! BLEEP! THE' + printableCommandString + ' CIRCUITS ARE NOW DEACTIVATED!!!')

            #reminder for chat users that chat commands exist after 70 messages
            time.sleep(2)
            reminderClock = reminderClock + 1
            print('reminder message count: ', reminderClock)
            if reminderClock == 70:
                sendMessage(s, 'Be sure to check out the chat command list below for visual glitches!')
                reminderClock = 0

finally:
    GPIO.cleanup()
    print('GlitchBot Shutdown')
    time.sleep(5)
    sendMessage(s, 'GlitchBot Shutdown BLEEP! BLORB!')
