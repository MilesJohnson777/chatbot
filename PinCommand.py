camCommands = {'switch': 'z-switch', #orange wire
               'UL': 'a-UL', #purple wire
               'UR': 'a-UR', #yellow wire
               'LL': 'a-LL', #green wire
               'LR': 'a-LR', #blue wire
               'destroy': 'b-destroy',
               'blur': 'b-blur',
              # 'fuzz': 'b-fuzz',
               'bwhc': 'b-BWHC',
               'buzz': 'b-buzz',
               'bifrost': 'b-bifrost',
               'hum': 'b-hum',
               'bizz': 'b-bizz',
               'smear': 'b-smear'}

relatedPins = {'z-switch': 18, #orange
               'a-UL': 27, #purple
               'a-UR': 17, #yellow
               'a-LL': 22, #green
               'a-LR': 4,  #blue
               'b-destroy': 24,
               'b-blur': 21,
              # 'b-fuzz': 20,
               'b-BWHC': 23,
               'b-buzz': 26,
               'b-bifrost': 5,
               'b-hum': 25,
               'b-bizz': 19,
               'b-smear': 16}

pinList = [18, 27, 17, 22, 4, 23, 24, 25, 5, 16, 19, 26, 21] # pin 20
