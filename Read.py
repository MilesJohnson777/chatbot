import string

def getMessage(line):
    try:
        if 'PING :tmi.twitch.tv' in line:
            print(line)
            return line
        separate = line.split(':', 2)
        message = separate[2]
        return message
    except IndexError as error:
        print(error)
        message = ['there was an error 666']
        return message
